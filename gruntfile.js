module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		ngAnnotate: {
			options: {
				singleQuotes: true
			},
			app: { //"app" target
				files: {
						'./Resources/views/Default/min/app.js': ['./Resources/views/Default/app/app.js'],
						'./Resources/views/Default/min/services.js': [
						'./Resources/views/Default/app/services/products.service.js',
						'./Resources/views/Default/app/services/firms.service.js',
						'./Resources/views/Default/app/services/offices.service.js',
						'./Resources/views/Default/app/services/clients.service.js',
						'./Resources/views/Default/app/services/records.service.js',
						'./Resources/views/Default/app/services/comments.service.js',
						'./Resources/views/Default/app/services/groups.service.js',
						'./Resources/views/Default/app/services/users.service.js'
					],
					'./Resources/views/Default/min/controllers.js': [
						'./Resources/views/Default/app/controllers/products.controller.js',
						'./Resources/views/Default/app/controllers/firms.controller.js',
						'./Resources/views/Default/app/controllers/offices.controller.js',
						'./Resources/views/Default/app/controllers/clients.controller.js',
						'./Resources/views/Default/app/controllers/records.controller.js',
						'./Resources/views/Default/app/controllers/comments.controller.js',
						'./Resources/views/Default/app/controllers/users.controller.js',
						'./Resources/views/Default/app/controllers/groups.controller.js'
					],
				}
			}
		},
		concat: {
			js: { //target
				src: ['./Resources/views/Default/min/*.js'],
				dest: '../../web/app/app.min.js'
			}
		},
		uglify: {
			js: { //target
				src: ['../../web/app/app.min.js'],
				dest: '../../web/app/app.min.js'
			}
		}



		//grunt task configuration will go here
	});
	//load grunt task
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-ng-annotate');
	//register grunt default task
	grunt.registerTask('default', ['ngAnnotate', 'concat', 'uglify']);

}