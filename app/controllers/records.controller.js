/**
 * Created by delev on 09.09.2016.
 */
(function(){
	'use strict';
	angular
			.module('sisApp')
			.controller('RecordsController', ['$scope', '$rootScope', '$log', '$stateParams', '$filter', 'RecordsService', 'ClientsService', 'FirmsService', 'ProductsService', 'OfficesService',
				function($scope, $rootScope, $log, $stateParams, $filter, RecordsService, ClientsService, FirmsService, ProductsService, OfficesService) {

					var self = this;
					self.sortType = 'timestamp';
					self.sortReverse = false;
					$scope.searchField = "";

					$scope.replace = function(arr, id, title){
						for (var i = 0, len = arr.length; i < len; i++) {
								if(parseInt(arr[i].id) === parseInt(id)){
									return arr[i][title];
								}
						}
					};

					$scope.status = [
							{
								id: "1",
								title: "Active"
							},
							{
								id: "2",
								title: "Finished"
							},
							{
								id: "3",
								title: "Irregular"
							},
							{
								id: "4",
								title: "Judicial"
							},
					];

					ClientsService.fetchClients("", function() {
						$scope.clients = ClientsService.getClients();
					});

					OfficesService.fetchOffices("", function() {
						$scope.offices = OfficesService.getOffices();
					});

					FirmsService.fetchFirms("", function() {
						$scope.firms = FirmsService.getFirms();
					});

					ProductsService.fetchProducts("", function() {
						$scope.products = ProductsService.getProducts();
					});

					$scope.changeFirm = function(id){
						$scope.productsList = $scope.products.filter(function(element){
								return element.firm === id;
						});
					}


					if($stateParams.id){
						RecordsService.fetchRecords($stateParams.id, function (data) {
							self.recordsList = RecordsService.getRecords();
							self.record =  self.recordsList[0];
						});
					}else{
						RecordsService.fetchRecords("", function (data) {
							self.recordsList = RecordsService.getRecords();
						});
					}

					$scope.putRecord = function(isValid, id) {
						if(isValid){
							var data = {
								'client': recordForm.client.value,
								'office': recordForm.office.value,
								'firm': recordForm.firm.value,
								'product': recordForm.product.value,
								'value': recordForm.value.value,
								'companyNumber': recordForm.company_number.value,
								'status': recordForm.status.value,
								'contractDate': $filter('date')(recordForm.contract_date.value, 'yyyy-MM-dd'),
							}
							RecordsService.putRecord(id, data)
						}
					}

					$scope.postRecord = function(isValid) {
						if(isValid){
							var data = {
								'client': recordForm.client.value,
								'office': recordForm.office.value,
								'firm': recordForm.firm.value,
								'product': recordForm.product.value,
								'value': recordForm.value.value,
								'companyNumber': recordForm.company_number.value,
								'status': recordForm.status.value,
								'contractDate': $filter('date')(recordForm.contract_date.value, 'yyyy-MM-dd'),
							}
							RecordsService.postRecord(data)
						}
					}


					$scope.records = self;
				}]);
})();