/**
 * Created by delev on 09.09.2016.
 */
(function(){
	'use strict';
	angular
			.module('sisApp')
			.controller('UsersController', ['$scope', '$rootScope', '$log', '$stateParams', 'UsersService', 'OfficesService',
				function($scope, $rootScope, $log, $stateParams, UsersService, OfficesService) {
					var self = this;
					$scope.users = [];
					$scope.users.offices = [];
					$scope.all_offices = [];

					self.findExistsInArray = function(arr, id) {
						return arr.some(function(element){
							return element === id;
						});
					}

					self.findIdInArray =  function(arr, id) {
						return arr.findIndex(function(element, index){
							return parseInt(element.id) === parseInt(id);
						});
					}

					OfficesService.fetchOffices("", function() {
						$scope.all_offices = OfficesService.getOffices();
					});

					$scope.checkPass = function(){
						$scope.userForm.new_password.$setValidity("new_password", false);
						$scope.userForm.confirm_password.$setValidity("confirm_password", false);
						if(userForm.new_password.value === userForm.confirm_password.value){
							$scope.userForm.new_password.$setValidity("new_password", true);
							$scope.userForm.confirm_password.$setValidity("confirm_password", true);
						}
					}

					UsersService.fetchUsers($stateParams.id, function(){
						$scope.users = UsersService.getUsers();
						if($scope.users.length == 1){

							$scope.addOffice = function(option){
								if(!self.findExistsInArray($scope.users.offices, option.g.id)){
									$scope.users.offices.push(option.g);
								}
							}

							$scope.removeOffice = function(option){
								$scope.users.offices.splice(self.findIdInArray($scope.users.offices, option.g.id), 1);
							}

							$scope.users = $scope.users[0];
							if($scope.users.roles.length > 1){
								$scope.users.roles = true;
							}else{
								$scope.users.roles = false;
							}
						}
					});



					$scope.postUser = function() {

						if(userForm.username.value !== "" && userForm.email.value !== ""){
							var data = {
								'username': userForm.username.value,
								'email': userForm.email.value,
								'password': userForm.new_password.value,
								'confirm_password': userForm.confirm_password.value,
							};

							UsersService.postUser(data, function(){
							});
						}
					}

					$scope.putUser = function() {
						UsersService.putUser($stateParams.id, $scope.users, function(){

						});

					}


					$scope.users = self;
				}]);
})();