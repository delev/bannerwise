/**
 * Created by delev on 09.09.2016.
 */
(function(){
	'use strict';
	angular
			.module('sisApp')
			.controller('FirmsController', ['$scope', '$log', '$stateParams', 'FirmsService',
				function($scope, $log, $stateParams, FirmsService) {
					var self = this;
					self.sortType = 'timestamp';
					self.sortReverse = false;
					$scope.searchField = "";

					if($stateParams.id){
						FirmsService.fetchFirms($stateParams.id, function () {
							self.firmsList = FirmsService.getFirms();
							self.firm = self.firmsList[0];
						});
					}else{
						FirmsService.fetchFirms("", function () {
							self.firmsList = FirmsService.getFirms();
						});
					}

					$scope.putFirm = function(isValid, id) {
						if(isValid){
							var data = {
								'name': firmForm.name.value,
								'manager': firmForm.manager.value,
								'phone': firmForm.phone.value,
								'email': firmForm.email.value,
								'comment': firmForm.comment.value,
							}
							FirmsService.putFirm(id, data)
						}
					}

					$scope.postFirm = function(isValid) {
						if(isValid){
							var data = {
								'name': firmForm.name.value,
								'manager': firmForm.manager.value,
								'phone': firmForm.phone.value,
								'email': firmForm.email.value,
								'comment': firmForm.comment.value,
							}
							FirmsService.postFirm(data)
						}
					}

					$scope.firms = self;
				}]);
})();