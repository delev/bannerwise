/**
 * Created by delev on 09.09.2016.
 */
(function(){
	'use strict';
	angular
			.module('sisApp')
			.controller('LogsController', ['$scope', '$log', '$stateParams', 'LogsService',
				function($scope, $log, $stateParams, LogsService) {
					var self = this;
					$scope.logs = [];


					LogsService.fetchLogs(function(){
						$scope.logs = LogsService.getLogs();
					});

					$scope.logs = self;
				}]);
})();