/**
 * Created by delev on 09.09.2016.
 */
(function(){
	'use strict';
		angular
				.module('sisApp')
				.controller('ProductsController', ['$scope', '$rootScope', '$log', '$stateParams', 'ProductsService', 'FirmsService',
					function($scope, $rootScope, $log, $stateParams, ProductsService, FirmsService) {
						var self = this;
						self.sortType = 'timestamp';
						self.sortReverse = false;
						$scope.searchField = "";


						FirmsService.fetchFirms("", function () {
							self.firmsList = FirmsService.getFirms();
						});

						if($stateParams.id){
							ProductsService.fetchProducts($stateParams.id, function () {
								self.productsList = ProductsService.getProducts();
								self.product = self.productsList[0];
							});
						}else{
							ProductsService.fetchProducts("", function () {
								self.productsList = ProductsService.getProducts();
							});
						}

						$scope.putProduct = function(isValid, id) {
							if(isValid){
									var data = {
											'name': productForm.name.value,
											'firm': productForm.firm.value,
											'details': productForm.details.value,
									}
									ProductsService.putProduct(id, data)
							}
						}

						$scope.postProduct = function(isValid) {
							if(isValid){
								var data = {
									'name': productForm.name.value,
									'firm': productForm.firm.value,
									'details': productForm.details.value,
								}
								ProductsService.postProduct(data)
							}
						}

						$scope.products = self;
				}]);
})();