/**
 * Created by delev on 09.09.2016.
 */
(function(){
	'use strict';
	angular
			.module('sisApp')
			.controller('CommentsController', ['$scope', '$log', '$stateParams', 'CommentsService',
				function($scope, $log, $stateParams, CommentsService) {
					var self = this;
					var client_id = "";
					$scope.comments = [];

					if($stateParams.id){
						client_id = $stateParams.id;

						CommentsService.fetchComments(client_id, function(){
							$scope.comments = CommentsService.getComments();
						});
					}

					$scope.postComment = function() {
						client_id = $stateParams.id;
						if(newMessageForm.message.value !== ""){
							var data = {
								'comment': newMessageForm.message.value,
								'clients': client_id,
								'client': client_id,
								'userId': 1,
							};

							CommentsService.postComment(data, function(back_data){
								$scope.comments.push(back_data);
							});
						}
					}


					$scope.comments = self;
				}]);
})();