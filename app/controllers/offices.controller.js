/**
 * Created by delev on 09.09.2016.
 */
(function(){
	'use strict';
	angular
			.module('sisApp')
			.controller('OfficesController', ['$scope', '$rootScope', '$log', '$stateParams', 'OfficesService',
				function($scope, $rootScope, $log, $stateParams, OfficesService) {
					var self = this;
					var office_id = "";
					$scope.offices = [];

					if($stateParams.id){
						OfficesService.fetchOffices($stateParams.id, function () {
							self.offices = OfficesService.getOffices();
							self.offices = self.offices[0];
						});
					}else{
						OfficesService.fetchOffices("", function () {
							self.offices = OfficesService.getOffices();
						});
					}

					$scope.postOffice = function() {

						if(officeForm.name.value !== ""){
							var data = {
								'name': officeForm.name.value,
								'manager': officeForm.manager.value,
								'location': officeForm.location.value,
								'phone': officeForm.phone.value,
								'email': officeForm.email.value,
								'comment': officeForm.comment.value,
							};

							OfficesService.postOffice(data, function(){
								window.open('/#/offices', '_self');
							});
						}
					}

					$scope.putOffice = function() {

						office_id = $stateParams.id;

						if(officeForm.name.value !== ""){
							var data = {
								'name': officeForm.name.value,
								'manager': officeForm.manager.value,
								'location': officeForm.location.value,
								'phone': officeForm.phone.value,
								'email': officeForm.email.value,
								'comment': officeForm.comment.value,
							};

							OfficesService.putOffice(office_id, data, function(){
								window.open('/#/offices', '_self');
							});
						}
					}


					$scope.offices = self;
				}]);
})();