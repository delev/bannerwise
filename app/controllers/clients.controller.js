/**
 * Created by delev on 09.09.2016.
 */
(function(){
	'use strict';
	angular
			.module('sisApp')
			.controller('ClientsController', ['$scope', '$log', '$stateParams', 'ClientsService', 'FirmsService', 'ProductsService', 'OfficesService',
				function($scope, $log, $stateParams, ClientsService, FirmsService, ProductsService, OfficesService) {
					// $log.info("ClientsController");
					var self = this;
					self.sortType = 'timestamp';
					self.sortReverse = false;
					$scope.searchField = "";

					$scope.selected = {};
					$scope.selected.firm = "";
					$scope.selected.offices = "";
					$scope.selected.product = "";


					$scope.replace = function(arr, id, title){
						for (var i = 0, len = arr.length; i < len; i++) {
							if(parseInt(arr[i].id) === parseInt(id)){
								return arr[i][title];
							}
						}
					};

					$scope.types = [
						{
							id: "1",
							title: "Person"
						},
						{
							id: "2",
							title: "Firm"
						},
						{
							id: "3",
							title: "Landlord"
						},
					];

					$scope.kid = [
						{ "id": "0", "title": "Person",},
						{ "id": "1", "title": "Firm",},
						{ "id": "2", "title": "Oil and gas production",},
						{ "id": "3", "title": "Tailor",},
					];

					$scope.typeFirm = false;
					$scope.addKid = function () {
						$scope.typeFirm = false;
						if(parseInt(clientForm.type.value)  === 2){
							$scope.typeFirm = true;
						}
					}

					// FILTERS
					function checkStatus(arr, val, type) {
						return arr.some(function(arrVal) {
							return parseInt(val[type]) === parseInt(arrVal[type]);
						});
					}

					function checkActivity(arr, val, type) {
						var from = new Date(val[type]).getUnixTime();
						var to = new Date().getUnixTime();

						return arr.some(function(element){
							var array_date = new Date(element[type]).getUnixTime();

							if(from <= array_date && array_date <= to){
								return true;
							}
							return false;
						});

					}

					$scope.filtering = function(type, subfilter){
						var filtered = [];
						if($scope.selected[type] !== ""){
							self.clientsList.forEach(function(element){

								if(subfilter === 'records'){
									if(element.records.length > 0){

										switch (type){
											case "status":
											default:
												if(checkStatus(element.records, $scope.selected, type)){
													filtered.push(element);
												}
												break;

											case "updated_at":
												if(checkActivity(element.records, $scope.selected, type)){
													filtered.push(element);
												}
												break;
										}

									}
								}else{
									if(element[type] == $scope.selected[type]){
										filtered.push(element);
									}
								}

							});
							self.allClients = filtered;

						}else{
							self.allClients = self.clientsList;
						}
					};
					// FILTERS

					$scope.download = function(){

						var csvContent = "data:text/csv;charset=utf-8,";
						csvContent += "Name,Email,Phone\r\n";

						self.allClients.forEach(function(element, index){
							csvContent +=  element.name + "," + element.email + "," + element.phone + "\r\n";
						});

						var encodedUri = encodeURI(csvContent);
						window.open(encodedUri);

					}

					FirmsService.fetchFirms("", function () {
						$scope.firms = FirmsService.getFirms();
					});

					OfficesService.fetchOffices("", function () {
						$scope.offices = OfficesService.getOffices();
					});

					ProductsService.fetchProducts("", function () {
						$scope.products = ProductsService.getProducts();
					});

					if($stateParams.id){
						ClientsService.fetchClients($stateParams.id, function () {
							self.clientsList = ClientsService.getClients();
							self.client = self.clientsList[0];
						});
					}else{
						ClientsService.fetchClients("", function () {
							self.clientsList = ClientsService.getClients();
							self.allClients = self.clientsList;
						});
					}

					$scope.putClient = function(isValid, id) {
						if(isValid){
							var data = {
								'name':       clientForm.name.value,
								'type':       clientForm.type.value,
								'number':     clientForm.number.value,
								'email':      clientForm.email.value,
								'phone':      clientForm.phone.value,
								'address':    clientForm.address.value,
								'city':       clientForm.city.value,
								'manager':    clientForm.manager.value,
								'middleman':  clientForm.middleman.value,
								'website':    clientForm.website.value,
								'information':clientForm.information.value,
							}

							if(clientForm.type.value === 2 && clientForm.kid.value !== ''){
								data['kid'] = clientForm.kid.value;
							}

							ClientsService.putClient(id, data)
						}
					}

					$scope.postClient = function(isValid) {

						if(isValid){
							var data = {
								'name':       clientForm.name.value,
								'type':       clientForm.type.value,
								'number':     clientForm.number.value,
								'email':      clientForm.email.value,
								'phone':      clientForm.phone.value,
								'address':    clientForm.address.value,
								'city':       clientForm.city.value,
								'manager':    clientForm.manager.value,
								'middleman':  clientForm.middleman.value,
								'website':    clientForm.website.value,
								'information':clientForm.information.value,
							}

							if(parseInt(data['type']) === 2 && clientForm.kid.value !== ''){
								data['kid'] = clientForm.kid.value;
							}

							ClientsService.postClient(data);
						}
					}

					$scope.clients = self;
				}]);
})();