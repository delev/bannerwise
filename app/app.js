/**
 * Created by delev on 09.09.2016.
 */

(function () {

	angular.module('sisApp', [
		'ngTable',
		'ui.router',
	]);

	angular
			.module('sisApp')
			.constant('CONSTANTS', {

				URL: "/app.php",
				REST_URL: "/app.php",
				URL_JS: "/",
				FORMAT: "json",

				//API: GET, POST, GET {entity}, PUT {entity}, PATCH {entity}, DELETE {entity}
				PRODUCTS: "/api/products",
				FIRMS:    "/api/firms",
				CLIENTS:  "/api/clients",
				RECORDS:  "/api/records",
				COMMENTS: "/api/comments",
				USERS:    "/api/users",
				OFFICES:  "/api/offices",
				LOGS:     "/api/logs",

				DATE: new Date(),
				DATE_FORMAT: "yyyy-MM-dd",
			});


	angular
			.module('sisApp')
			.config(function($stateProvider, $urlRouterProvider, CONSTANTS) {

				$stateProvider

						.state('home', {
							url: "/",
							templateUrl: CONSTANTS.URL_JS + "app/views/index.html"
						})

						.state('records', {
							url: "/records",
							templateUrl: CONSTANTS.URL_JS + "app/views/records.html"
						})
						.state('add_record', {
							url: "/add_record",
							templateUrl: CONSTANTS.URL_JS + "app/views/add_record.html"
						})
						.state('edit_record', {
							url: "/edit_record/:id",
							templateUrl: CONSTANTS.URL_JS + "app/views/edit_record.html",
							controller: "RecordsController",
						})
						.state('view_record', {
							url: "/view_record/:id",
							templateUrl: CONSTANTS.URL_JS + "app/views/view_record.html",
							controller: "RecordsController",
						})

						.state('clients', {
							url: "/clients",
							templateUrl: CONSTANTS.URL_JS + "app/views/clients.html"
						})
						.state('add_client', {
							url: "/add_client",
							templateUrl: CONSTANTS.URL_JS + "app/views/add_client.html"
						})
						.state('edit_client', {
							url: "/edit_client/:id",
							templateUrl: CONSTANTS.URL_JS + "app/views/edit_client.html",
							controller: "ClientsController",
						})
						.state('view_client', {
							url: "/view_client/:id",
							templateUrl: CONSTANTS.URL_JS + "app/views/view_client.html",
							controller: "ClientsController",
						})

						.state('firms', {
							url: "/firms",
							templateUrl: CONSTANTS.URL_JS + "app/views/firms.html"
						})
						.state('add_firm', {
							url: "/add_firm",
							templateUrl: CONSTANTS.URL_JS + "app/views/add_firm.html"
						})
						.state('edit_firm', {
							url: "/firms/:id",
							templateUrl: CONSTANTS.URL_JS + "app/views/edit_firm.html",
							controller: "FirmsController",
						})


						.state('products', {
							url: "/products",
							templateUrl: CONSTANTS.URL_JS + "app/views/products.html"
						})
						.state('add_product', {
							url: "/add_product",
							templateUrl: CONSTANTS.URL_JS + "app/views/add_product.html"
						})
						.state('edit_product', {
							url: "/products/:id",
							templateUrl: CONSTANTS.URL_JS + "app/views/edit_product.html",
							controller: "ProductsController",
						})

						.state('users', {
							url: "/users",
							templateUrl: CONSTANTS.URL_JS + "app/views/users.html"
						})
						.state('add_user', {
							url: "/add_user",
							templateUrl: CONSTANTS.URL_JS + "app/views/add_user.html"
						})
						.state('edit_user', {
							url: "/users/:id",
							templateUrl: CONSTANTS.URL_JS + "app/views/edit_user.html",
							controller: "UsersController",
						})

						.state('offices', {
							url: "/offices",
							templateUrl: CONSTANTS.URL_JS + "app/views/offices.html"
						})
						.state('add_office', {
							url: "/add_office",
							templateUrl: CONSTANTS.URL_JS + "app/views/add_office.html"
						})
						.state('edit_office', {
							url: "/offices/:id",
							templateUrl: CONSTANTS.URL_JS + "app/views/edit_office.html",
							controller: "OfficesController",
						})

						.state('logs', {
							url: "/logs",
							templateUrl: CONSTANTS.URL_JS + "app/views/logs.html"
						});

				$urlRouterProvider.otherwise("/");

			});

})();