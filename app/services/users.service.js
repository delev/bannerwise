/**
 * Created by delev on 09.09.2016.
 */

(function () {
	'use strict';

	angular
			.module('sisApp')
			.factory('UsersService', ['$rootScope', '$http', '$log', 'CONSTANTS',
				function ($rootScope, $http, $log, CONSTANTS) {
					var users = [];
					var url = "";

					self.fetchUsers = function (user_id, cb) {
						if(user_id){
							url = CONSTANTS.REST_URL + CONSTANTS.USERS + "." + CONSTANTS.FORMAT  + "?filters[id]=" + user_id;
						}else{
							url = CONSTANTS.REST_URL + CONSTANTS.USERS + "." + CONSTANTS.FORMAT;
						}

						return $http.get(url)
								.then(
										function (response) {
											users = response.data;
											cb();
										},
										function () {
											users = [];
											cb();
										});
					};


					self.getUsers = function () {
						return users;
					};


					self.postUser = function (data, cb) {

						var req = {
							method: 'POST',
							url: CONSTANTS.REST_URL + CONSTANTS.USERS + "." + CONSTANTS.FORMAT,
							data: data
						};

						$http(req).then(
								function (response) {
									UIkit.notify({
										message : 'User Added!',
										status  : 'success',
										timeout : 5000,
										pos     : 'top-center'
									});
									window.open('/#/users', '_self');
									cb(response.data);
								}, function(error){
									UIkit.notify({
										message : error.data.errors,
										status  : 'danger',
										timeout : 5000,
										pos     : 'top-center'
									});
								});
					};

					self.putUser = function (id, data) {

						var req = {
							method: 'PUT',
							url: CONSTANTS.REST_URL + CONSTANTS.USERS + "/" + id + "." + CONSTANTS.FORMAT,
							data: data
						};

						$http(req).then(
								function (response) {
									UIkit.notify({
										message : 'User Updated!',
										status  : 'success',
										timeout : 5000,
										pos     : 'top-center'
									});
									window.open('/#/users', '_self');
								}, function(error){
									UIkit.notify({
										message : 'User Error!',
										status  : 'danger',
										timeout : 5000,
										pos     : 'top-center'
									});
								});
					};

					// API
					return {
						fetchUsers : self.fetchUsers,
						getUsers : self.getUsers,
						putUser : self.putUser,
						postUser : self.postUser,
					};

				}]);
}());