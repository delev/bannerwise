/**
 * Created by delev on 09.09.2016.
 */

(function () {
	'use strict';

	angular
			.module('sisApp')
			.factory('FirmsService', ['$rootScope', '$http', '$log', 'CONSTANTS',
				function ($rootScope, $http, $log, CONSTANTS) {
					var firms = [];

					self.fetchFirms = function (id, cb) {
						var url = CONSTANTS.REST_URL + CONSTANTS.FIRMS + "." + CONSTANTS.FORMAT;
						if(id !== ""){
							url = CONSTANTS.REST_URL + CONSTANTS.FIRMS + "." + CONSTANTS.FORMAT + "?filters[id]=" + id;
						}

						return $http.get(url)
								.then(
										function (response) {
											firms = response.data;
											cb();
										},
										function () {
											firms = [];
											cb();
										});
					};


					self.getFirms = function () {
						return firms;
					};

					self.putFirm = function (id, data) {

						var req = {
							method: 'PUT',
							url: CONSTANTS.REST_URL + CONSTANTS.FIRMS + "/" + id + "." + CONSTANTS.FORMAT,
							data: data
						};

						$http(req).then(
								function (response) {
									UIkit.notify({
										message : 'Firm Updated!',
										status  : 'success',
										timeout : 5000,
										pos     : 'top-center'
									});
									window.open('/#/firms', '_self');
								}, function(error){
									UIkit.notify({
										message : 'Firm Error!',
										status  : 'danger',
										timeout : 5000,
										pos     : 'top-center'
									});
								});
					};

					self.postFirm = function (data) {

						var req = {
							method: 'POST',
							url: CONSTANTS.REST_URL + CONSTANTS.FIRMS + "." + CONSTANTS.FORMAT,
							data: data
						};

						$http(req).then(
								function (response) {
									UIkit.notify({
										message : 'Firm Added!',
										status  : 'success',
										timeout : 5000,
										pos     : 'top-center'
									});
									window.open('/#/firms', '_self');
								}, function(error){
									UIkit.notify({
										message : 'Firm Error!',
										status  : 'danger',
										timeout : 5000,
										pos     : 'top-center'
									});
								});
					};

					// API
					return {
						fetchFirms : self.fetchFirms,
						getFirms : self.getFirms,
						putFirm : self.putFirm,
						postFirm : self.postFirm,
					};

				}]);
}());
