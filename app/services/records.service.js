/**
 * Created by delev on 09.09.2016.
 */

(function () {
	'use strict';

	angular
			.module('sisApp')
			.factory('RecordsService', ['$rootScope', '$http', '$log', 'CONSTANTS',
				function ($rootScope, $http, $log, CONSTANTS) {
					var records = [];

					self.fetchRecords = function (id, cb) {
						var url = CONSTANTS.REST_URL + CONSTANTS.RECORDS + "." + CONSTANTS.FORMAT;
						if(id !== ""){
								url = CONSTANTS.REST_URL + CONSTANTS.RECORDS + "." + CONSTANTS.FORMAT + "?filters[id]=" + id;
						}

						return $http.get(url)
								.then(
										function (response) {
											records = response.data;
											cb();
										},
										function () {
											records = [];
											cb();
										});
					};

					self.getRecords = function () {
						return records;
					};

					self.putRecord = function (id, data) {

						var req = {
							method: 'PUT',
							url: CONSTANTS.REST_URL + CONSTANTS.RECORDS + "/" + id + "." + CONSTANTS.FORMAT,
							data: data
						};

						$http(req).then(
								function (response) {
									UIkit.notify({
										message : 'Record Updated!',
										status  : 'success',
										timeout : 5000,
										pos     : 'top-center'
									});
									window.open('/#/records', '_self');
								}, function(error){
									UIkit.notify({
										message : 'Record Error!',
										status  : 'danger',
										timeout : 5000,
										pos     : 'top-center'
									});
								});
					};

					self.postRecord = function (data) {

						var req = {
							method: 'POST',
							url: CONSTANTS.REST_URL + CONSTANTS.RECORDS + "." + CONSTANTS.FORMAT,
							data: data
						};

						$http(req).then(
								function (response) {
									UIkit.notify({
										message : 'Record Added!',
										status  : 'success',
										timeout : 5000,
										pos     : 'top-center'
									});
									window.open('/#/records', '_self');
								}, function(error){
									UIkit.notify({
										message : 'Record Error!',
										status  : 'danger',
										timeout : 5000,
										pos     : 'top-center'
									});
								});
					};

					// API
					return {
						fetchRecords : self.fetchRecords,
						getRecords : self.getRecords,
						putRecord : self.putRecord,
						postRecord : self.postRecord,
					};

				}]);
}());
