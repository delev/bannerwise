/**
 * Created by delev on 09.09.2016.
 */

(function () {
	'use strict';

	angular
			.module('sisApp')
			.factory('ProductsService', ['$rootScope', '$http', '$log', 'CONSTANTS',
				function ($rootScope, $http, $log, CONSTANTS) {
					var products = [];

					self.fetchProducts = function (id, cb) {
						var url = CONSTANTS.REST_URL + CONSTANTS.PRODUCTS + "." + CONSTANTS.FORMAT;
						if(id !== ""){
							url = CONSTANTS.REST_URL + CONSTANTS.PRODUCTS + "." + CONSTANTS.FORMAT + "?filters[id]=" + id;
						}

						return $http.get(url)
								.then(
										function (response) {
											products = response.data;
											cb();
										},
										function () {
											products = [];
											cb();
										});
					};


			self.getProducts = function () {
				return products;
			};

			self.putProduct = function (id, data) {

				var req = {
					method: 'PUT',
					url: CONSTANTS.REST_URL + CONSTANTS.PRODUCTS + "/" + id + "." + CONSTANTS.FORMAT,
					data: data
				};

				$http(req).then(
						function (response) {
							UIkit.notify({
								message : 'Product Updated!',
								status  : 'success',
								timeout : 5000,
								pos     : 'top-center'
							});
							window.open('/#/products', '_self');
						}, function(error){
							UIkit.notify({
								message : 'Product Error!',
								status  : 'danger',
								timeout : 5000,
								pos     : 'top-center'
							});
						});
			};

			self.postProduct = function (data) {

				var req = {
					method: 'POST',
					url: CONSTANTS.REST_URL + CONSTANTS.PRODUCTS + "." + CONSTANTS.FORMAT,
					data: data
				};

				$http(req).then(
						function (response) {
							UIkit.notify({
								message : 'Product Added!',
								status  : 'success',
								timeout : 5000,
								pos     : 'top-center'
							});
							window.open('/#/products', '_self');
						}, function(error){
							UIkit.notify({
								message : 'Product Error!',
								status  : 'danger',
								timeout : 5000,
								pos     : 'top-center'
							});
						});
			};

			// API
			return {
				fetchProducts : self.fetchProducts,
				getProducts : self.getProducts,
				putProduct : self.putProduct,
				postProduct : self.postProduct,
			};

		}]);
}());
