/**
 * Created by delev on 09.09.2016.
 */

(function () {
	'use strict';

	angular
			.module('sisApp')
			.factory('OfficesService', ['$rootScope', '$http', '$log', 'CONSTANTS',
				function ($rootScope, $http, $log, CONSTANTS) {
					var offices = [];
					var url = "";

					self.fetchOffices = function (office_id, cb) {
						if(office_id){
							url = CONSTANTS.REST_URL + CONSTANTS.OFFICES + "." + CONSTANTS.FORMAT  + "?filters[id]=" + office_id;
						}else{
							url = CONSTANTS.REST_URL + CONSTANTS.OFFICES + "." + CONSTANTS.FORMAT;
						}

						return $http.get(url)
								.then(
										function (response) {
											offices = response.data;
											cb();
										},
										function () {
											offices = [];
											cb();
										});
					};


					self.getOffices = function () {
						return offices;
					};

					self.putOffice = function (id, data) {

						var req = {
							method: 'PUT',
							url: CONSTANTS.REST_URL + CONSTANTS.OFFICES + "/" + id + "." + CONSTANTS.FORMAT,
							data: data
						};

						$http(req).then(
								function (response) {
									UIkit.notify({
										message : 'Office Updated!',
										status  : 'success',
										timeout : 5000,
										pos     : 'top-center'
									});
								}, function(error){
									UIkit.notify({
										message : 'Office Error!',
										status  : 'danger',
										timeout : 5000,
										pos     : 'top-center'
									});
								});
					};

					self.postOffice = function (data, cb) {

						var req = {
							method: 'POST',
							url: CONSTANTS.REST_URL + CONSTANTS.OFFICES + "." + CONSTANTS.FORMAT,
							data: data
						};

						$http(req).then(
								function (response) {
									UIkit.notify({
										message : 'Office Added!',
										status  : 'success',
										timeout : 5000,
										pos     : 'top-center'
									});
									cb(response.data);
								}, function(error){
									UIkit.notify({
										message : 'Office Error!',
										status  : 'danger',
										timeout : 5000,
										pos     : 'top-center'
									});
								});
					};

					// API
					return {
						fetchOffices : self.fetchOffices,
						getOffices : self.getOffices,
						putOffice : self.putOffice,
						postOffice : self.postOffice,
					};

				}]);
}());