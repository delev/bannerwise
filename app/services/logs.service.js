/**
 * Created by delev on 09.09.2016.
 */

(function () {
	'use strict';

	angular
			.module('sisApp')
			.factory('LogsService', ['$rootScope', '$http', '$log', 'CONSTANTS',
				function ($rootScope, $http, $log, CONSTANTS) {
					var logs = [];
					var url = "";

					self.fetchLogs = function (cb) {

							url = CONSTANTS.REST_URL + CONSTANTS.LOGS + "." + CONSTANTS.FORMAT;

						return $http.get(url)
								.then(
										function (response) {
											logs = response.data;
											cb();
										},
										function () {
											logs = [];
											cb();
										});
					};


					self.getLogs = function () {
						return logs;
					};


					self.postLog = function (user_id, action) {

						var req = {
							method: 'POST',
							url: CONSTANTS.REST_URL + CONSTANTS.LOGS + "." + CONSTANTS.FORMAT,
							data: {
								"user_id": user_id,
								"action": action
							}
						};

						$http(req).then(
								function (response) {
									$log.info("Log Added");
								}, function(error){
									$log.info("Error ", error);
								});
					};

					// API
					return {
						fetchLogs : self.fetchLogs,
						getLogs : self.getLogs,
					};

				}]);
}());