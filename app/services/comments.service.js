/**
 * Created by delev on 09.09.2016.
 */

(function () {
	'use strict';

	angular
			.module('sisApp')
			.factory('CommentsService', ['$rootScope', '$http', '$log', 'CONSTANTS',
				function ($rootScope, $http, $log, CONSTANTS) {
					var comments = [];

					self.fetchComments = function (client_id, cb) {

						return $http.get(CONSTANTS.REST_URL + CONSTANTS.COMMENTS + "." + CONSTANTS.FORMAT  + "?filters[client]=" + client_id)
								.then(
										function (response) {
											comments = response.data;
											cb();
										},
										function () {
											comments = [];
											cb();
										});
					};


					self.getComments = function () {
						return comments;
					};

					self.postComment = function (data, cb) {

						var req = {
							method: 'POST',
							url: CONSTANTS.REST_URL + CONSTANTS.COMMENTS + "." + CONSTANTS.FORMAT,
							data: data
						};

						$http(req).then(
								function (response) {
									UIkit.notify({
										message : 'Comment Added!',
										status  : 'success',
										timeout : 5000,
										pos     : 'top-center'
									});
									cb(response.data);
								}, function(error){
									UIkit.notify({
										message : 'Comment Error!',
										status  : 'danger',
										timeout : 5000,
										pos     : 'top-center'
									});
								});
					};

					// API
					return {
						fetchComments : self.fetchComments,
						getComments : self.getComments,
						postComment : self.postComment,
					};

				}]);
}());