/**
 * Created by delev on 09.09.2016.
 */

(function () {
	'use strict';

	angular
			.module('sisApp')
			.factory('ClientsService', ['$rootScope', '$http', '$log', 'CONSTANTS',
				function ($rootScope, $http, $log, CONSTANTS) {
					var clients = [];

					self.fetchClients = function (id, cb) {
						var url = CONSTANTS.REST_URL + CONSTANTS.CLIENTS + "." + CONSTANTS.FORMAT;
						if(id !== ""){
							url = CONSTANTS.REST_URL + CONSTANTS.CLIENTS + "." + CONSTANTS.FORMAT + "?filters[id]=" + id;
						}

						return $http.get(url)
								.then(
										function (response) {
											clients = response.data;
											cb();
										},
										function () {
											clients = [];
											cb();
										});
					};


					self.getClients = function () {
						return clients;
					};

					self.putClient = function (id, data) {
						var req = {
							method: 'PUT',
							url: CONSTANTS.REST_URL + CONSTANTS.CLIENTS + "/" + id + "." + CONSTANTS.FORMAT,
							data: data
						};

						$http(req).then(
								function (response) {
									UIkit.notify({
										message : 'Client Updated!',
										status  : 'success',
										timeout : 5000,
										pos     : 'top-center'
									});
									window.open('/#/clients', '_self');
								}, function(error){
									UIkit.notify({
										message : 'Client Error!',
										status  : 'danger',
										timeout : 5000,
										pos     : 'top-center'
									});
								});
					};

					self.postClient = function (data) {

						var req = {
							method: 'POST',
							url: CONSTANTS.REST_URL + CONSTANTS.CLIENTS + "." + CONSTANTS.FORMAT,
							data: data
						};

						$http(req).then(
								function (response) {
									UIkit.notify({
										message : 'Client Added!',
										status  : 'success',
										timeout : 5000,
										pos     : 'top-center'
									});
									window.open('/#/clients', '_self');
								}, function(error){
									UIkit.notify({
										message : 'Client Error!',
										status  : 'danger',
										timeout : 5000,
										pos     : 'top-center'
									});
								});
					};

					// API
					return {
						fetchClients : self.fetchClients,
						getClients : self.getClients,
						putClient : self.putClient,
						postClient : self.postClient,
					};

				}]);
}());
