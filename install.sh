#!/bin/bash

echo "##### BUILDING JS APP ... #####"
echo ""
grunt
cp -rfv Resources/views/Default/app/views ../../web/app/

echo "##### BOWER INSTALL #####"
echo ""
bower install
echo "##### REMOVING CACHE #####"
echo ""
../../bin/console cache:clear
../../bin/console cache:warmup
